package main;

public class TennisGame {

    private static final String DEUCE = "Deuce";
    private static final String WINFOR = "Win for ";
    private static final String ADVANTAGE = "Advantage ";
    private static final String ALL = "All";
    private static final String SPACER = "-";

    private static final String[] scoresTranslated = new String[]{"Love", "Fifteen", "Thirty", "Forty"};

    private int scorePlayer1;
    private int scorePlayer2;
    private String player1;
    private String player2;

    public TennisGame(String player1, String player2) {
        this.player1 = player1;
        this.player2 = player2;
    }


    public String getScore() {

        if (scorePlayer1 == scorePlayer2) {
            if(scorePlayer1 < 3) {
                return (scoresTranslated[scorePlayer1] + SPACER + ALL);
            } else {
                return (DEUCE);
            }
        }
        

        if (scorePlayer1 < 4 && scorePlayer2 < 4) {
            return (scoresTranslated[scorePlayer1] + SPACER + scoresTranslated[scorePlayer2]);
        }
        
        
        if (scorePlayer1 +1  < scorePlayer2 && scorePlayer2 > 3) {
            return (WINFOR + player2);
        }
        if (scorePlayer1 > scorePlayer2 +1 && scorePlayer1 > 3) {
            return (WINFOR + player1);
        }
        
        
        if (scorePlayer1 < scorePlayer2) {
            return (ADVANTAGE + player2);
        }
        return (ADVANTAGE + player1);
    }

    public void wonPoint(String player) {
        if (player.equals(player1)) {
            this.scorePlayer1++;
        } else {
            this.scorePlayer2++;
        }
    }

}
